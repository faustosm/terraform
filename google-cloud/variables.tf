variable "project_id" {
  type    = "string"
  default = "cluster-k8s-ansible-01"
}

variable "regiao" {
  type = "string"
  default = "us-central1"
}

variable "nome" {
  type = "string"
  default = "servidor"
}

variable "tipo_maquina" {
  type = "string"
  default = "n1-standard-2"
}

variable "zona" {
  type = "string"
  default = "us-central1-a"
}

variable "imagem" {
  type = "string"
  default = "debian-cloud/debian-9"
}
