# Configura o projeto GCP
provider "google" {
  credentials = "${file("cluster-k8s-ansible-01-2d3c4de55fa5.json")}"
  project     = "${var.project_id}"
  region      = "${var.regiao}"
}

# Cria a VM com o Google Compute Engine
resource "google_compute_instance" "webserver" {
  name          = "${var.nome}"
  machine_type  = "${var.tipo_maquina}"
  zone          = "${var.zona}"

  boot_disk {
    initialize_params {
      image = "${var.imagem}"
    }
  }

  # Instala pacotes basicos
  metadata_startup_script = "sudo apt-get update; sudo apt-get install nano git"

  # Habilita rede para a VM bem como um IP público
  network_interface {
    network = "default"
    access_config {

    }
  }
}
